	//设置根字体大小
	function rootFont () {
		var oHtml = document.getElementsByTagName('html')[0];
		var viewWidth = document.documentElement.offsetWidth || document.body.offsetWidth;
		oHtml.style.fontSize = viewWidth/10 + "px";
	}
	rootFont();
	window.onresize = function () {
		rootFont();
	}
	//弹窗js
	;(function ($) {
		var Dialog = function (config) {
			console.log("test1");
			var _this = this;
			//默认配置参数
			this.config = {
				//对话框宽高
				width:"auto",
				height:"auto",
				//对话框遮罩透明度
				maskOpacity:null,
				//对话框类型
				type:"iconWaiting",
				//对话框提示信息
				message:null,
				//按钮配置
				buttons:null,
				//弹出框延迟关闭时间
				delay:null
			};
			if(config && $.isPlainObject(config)){
				  $.extend(this.config,config);
			}else {
				this.isConfig = true;
			};
			this.body = $('body');
			this.mask = $('<div class="dialog">');
			this.wrap = $('<div class="wrap">');
			this.content = $('<div class="content">');
			this.icon = $('<div class="icon">');
			this.message = $('<p class="message">');
			this.btnGroup = $('<div class="footer">');
			//渲染DOM
			this.create();
		}
		Dialog.prototype = {
			//创建弹出框
			create:function () {
				var _this = this,
				config = this.config,
				mask = this.mask,
				wrap = this.wrap,
				content = this.content,
				icon = this.icon,
				message = this.message,
				btnGroup = this.btnGroup,
				body = this.body
				//插入内容到页面
				body.css('overflow','hidden');
				if(this.isConfig){
					body.append(mask);
					mask.append(icon.addClass('iconWaiting'));
					body.css('overflow','hidden');
					icon.on('tap',function () {
						mask.css('display','none');
					})
				}else{
					body.append(mask);
					mask.append(wrap);
					wrap.append(content);
					wrap.append(btnGroup);
					content.append(icon);
					//设置弹窗类型
					if(config.type){
						icon.addClass(config.type);
					}
					//设置提示文本
					if(config.message){
						message.html(config.message);
						content.append(message);
					}
					//设置弹窗宽度
					if(config.width != "auto"){
						wrap.width(config.width);
					}
					//设置遮罩层透明度
					if(config.maskOpacity){
						mask.css('background',"rgba(0,0,0," + config.maskOpacity + ")");
					}
					//设置延迟消失时间
					if(config.delay && config.delay != 0){
						setTimeout(function () {
							$(mask).hide();
							body.css('overflow','visible');
						},config.delay);
					}
					//设置按钮组
					if(config.buttons){
						this.createButtons(btnGroup,config.buttons);
					}
				}
			},
			//根据配置参数的buttons信息创建按钮
			createButtons:function (footer,buttons) {
				console.log(buttons);
				$(buttons).each(function () {
					var btn = $('<span class="btn ' + this.type + '">');
					var callBack = this.callback?this.callback:null;
					if(callBack){
						/*btn.on('click',function () {
							callBack();
						})*/
						btn.tap(function () {
							console.log(1212);
						})
					}
					btn.text(this.text);
					footer.append(btn);
				})

				/*
				 type:"iconWaiting",
				 text:"loading ┅┅",
				 callback:function () {

				 }
				 },*/
			}

		};
		window.Dialog = Dialog;
	})(Zepto); 